/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thugueno <thugueno@student.42angoulem      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/10 20:59:53 by thugueno          #+#    #+#             */
/*   Updated: 2023/06/30 17:48:09 by thugueno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHILO_H
# define PHILO_H

# include <unistd.h>
# include <string.h>
# include <stdio.h>
# include <stdlib.h>
# include <sys/time.h>
# include <pthread.h>

# define MAX_INT 214783647
# define MAX_PHILO 512
# define MAX_TIME 60000
# define ERRCOLOR "\033\001[31m"
# define NOCOLOR "\033\001[0m"

typedef struct s_dead
{
	pthread_mutex_t	mutex;
	int				val;
}	t_dead;

typedef struct s_leat
{
	pthread_mutex_t	mutex;
	long			val;
}	t_leat;

typedef struct s_param
{
	int				nb_philo;
	int				t_todie;
	int				t_toeat;
	int				t_tosleep;
	int				musteat;
	long			start;
	pthread_mutex_t	voice;
	t_dead			philof;
}	t_param;

typedef struct s_philo
{
	int				id;
	pthread_t		thread;
	t_param			*param;
	t_dead			isdead;
	int				t_eaten;
	t_leat			lasteat;
	pthread_mutex_t	*lfork;
	pthread_mutex_t	rfork;
}	t_philo;

typedef struct s_observer
{
	t_param	*param;
	t_philo	*philos;
}	t_observer;

/*	INIT	*/

t_observer	*init(int argc, char **argv);

/*	PHILO	*/

void		*philo_routine(void *arg);
int			isdead(t_philo *philo);

/*	THREAD	*/

void		erase_threads(t_philo *philos, int max, int state);
void		join_threads(t_philo *philos, int nb_philo);
int			create_threads(t_philo *philos, int nb_philo);
void		observer_routine(t_observer *obs);

/*	UTILS	*/

int			ft_atoi(char *nb);
void		*ft_calloc(int nmemb, int size);
long		get_timestamp_ms(void);

/*	PRINT	*/

void		ft_putchar_fd(int fd, char c);
void		ft_putnbr_fd(int fd, long n);
void		ft_putstr_fd(int fd, char *str);
void		ft_putendl_fd(int fd, char *str);
void		print_full(t_param *param);
void		print_death(t_param *param, int philo);
void		print_state(char *str, t_philo *philo);

/*	ERRORS	*/

void		print_usage(char *name);
void		print_error(t_param *param, int argc);

/*	EXIT	*/

void		free_philos(t_philo *philos, int limit);
void		free_observer(t_observer *observer);

#endif
