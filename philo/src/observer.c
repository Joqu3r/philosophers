/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   observer.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thugueno <thugueno@student.42angoulem      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/29 08:34:12 by thugueno          #+#    #+#             */
/*   Updated: 2023/06/30 23:59:56 by thugueno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

static void	kill_philos(t_philo *philos, int nb_philo)
{
	int	i;

	i = 0;
	while (i < nb_philo)
	{
		pthread_mutex_lock(&philos[i].isdead.mutex);
		philos[i].isdead.val = 1;
		pthread_mutex_unlock(&philos[i].isdead.mutex);
		i++;
	}
}

static int	check_philo_death(t_philo *philos, t_param *param)
{
	long	time;
	int		i;

	i = 0;
	while (i < param->nb_philo)
	{
		pthread_mutex_lock(&philos[i].lasteat.mutex);
		time = get_timestamp_ms() - (param->start + philos[i].lasteat.val);
		pthread_mutex_unlock(&philos[i].lasteat.mutex);
		if (time >= param->t_todie)
		{
			kill_philos(philos, param->nb_philo);
			pthread_mutex_lock(&param->voice);
			print_death(param, i + 1);
			pthread_mutex_unlock(&param->voice);
			return (1);
		}
		i++;
	}
	return (0);
}

static int	check_philo_eat(t_philo *philos, t_param *param)
{
	pthread_mutex_lock(&param->philof.mutex);
	if (param->philof.val == param->nb_philo)
	{
		pthread_mutex_unlock(&param->philof.mutex);
		kill_philos(philos, param->nb_philo);
		pthread_mutex_lock(&param->voice);
		print_full(param);
		pthread_mutex_unlock(&param->voice);
		return (1);
	}
	pthread_mutex_unlock(&param->philof.mutex);
	return (0);
}

void	observer_routine(t_observer *obs)
{
	int	state;

	state = 0;
	while (1)
	{
		state = check_philo_death(obs->philos, obs->param);
		if (state == 1)
			break ;
		if (obs->param->musteat > 0)
		{
			state = check_philo_eat(obs->philos, obs->param);
			if (state == 1)
				break ;
		}
	}
}
