/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thugueno <thugueno@student.42angouleme.fr  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/27 13:39:15 by thugueno          #+#    #+#             */
/*   Updated: 2023/07/01 17:28:27 by thugueno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

static void	init_left_fork(t_philo *philos, int nb_philo)
{
	int	i;

	i = 0;
	while (i < nb_philo)
	{
		if (nb_philo > 1 && i == 0)
			philos[i].lfork = &(philos[nb_philo - 1].rfork);
		else if (nb_philo > 1)
			philos[i].lfork = &(philos[i - 1].rfork);
		i++;
	}
}

static t_philo	*init_philos(t_param *param)
{
	t_philo	*philos;
	int		i;

	philos = ft_calloc(param->nb_philo, sizeof(*philos));
	if (!philos)
		return (philos);
	i = 0;
	while (i < param->nb_philo)
	{
		philos[i].param = param;
		philos[i].id = i + 1;
		pthread_mutex_init(&philos[i].rfork, NULL);
		pthread_mutex_init(&philos[i].isdead.mutex, NULL);
		pthread_mutex_init(&philos[i].lasteat.mutex, NULL);
		i++;
	}
	init_left_fork(philos, param->nb_philo);
	return (philos);
}

static int	check_param(t_param *param, int argc)
{
	print_error(param, argc);
	if (param->nb_philo > MAX_PHILO || param->nb_philo == 0)
		return (1);
	if ((param->t_todie > MAX_TIME || param->t_todie == 0)
		|| (param->t_toeat > MAX_TIME || param->t_toeat == 0)
		|| (param->t_tosleep > MAX_TIME || param->t_tosleep == 0))
		return (1);
	if (argc == 6 && param->musteat == 0)
		return (1);
	return (0);
}

static t_param	*init_param(int argc, char **argv)
{
	t_param	*param;

	param = ft_calloc(1, sizeof(*param));
	if (!param)
		return (param);
	param->nb_philo = ft_atoi(argv[1]);
	param->t_todie = ft_atoi(argv[2]);
	param->t_toeat = ft_atoi(argv[3]);
	param->t_tosleep = ft_atoi(argv[4]);
	if (argc == 6)
		param->musteat = ft_atoi(argv[5]);
	return (param);
}

t_observer	*init(int argc, char **argv)
{
	t_observer	*observer;

	observer = ft_calloc(1, sizeof(*observer));
	if (!observer)
		return (observer);
	observer->param = init_param(argc, argv);
	if (!observer->param || check_param(observer->param, argc))
	{
		free_observer(observer);
		return (NULL);
	}
	pthread_mutex_init(&(observer->param->voice), NULL);
	pthread_mutex_init(&(observer->param->philof.mutex), NULL);
	observer->philos = init_philos(observer->param);
	if (!observer->philos)
	{
		free_observer(observer);
		return (NULL);
	}
	return (observer);
}
