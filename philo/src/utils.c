/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thugueno <thugueno@student.42angouleme.fr  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/11 01:05:03 by thugueno          #+#    #+#             */
/*   Updated: 2023/06/05 22:46:05 by thugueno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

int	ft_atoi(char *nb)
{
	long	ret;
	int		digit;

	while ((*nb >= 8 && *nb <= 13) || *nb == ' ')
		nb++;
	ret = 0;
	digit = 0;
	if (*nb == '-')
		return (ret);
	if (*nb == '+')
		nb++;
	while (*nb == '0' && *nb + 1 != '\0')
		nb++;
	while (*nb >= '0' && *nb <= '9')
	{
		ret = ret * 10 + (*nb - '0');
		nb++;
		digit++;
	}
	if (*nb)
		return (0);
	if (digit > 10 || ret > MAX_INT)
		return (0);
	return ((int)ret);
}

void	*ft_calloc(int nmemb, int size)
{
	int		bytes;
	void	*ptr;

	if (size == 0 || nmemb == 0)
	{
		ptr = malloc(0);
		return (ptr);
	}
	bytes = nmemb * size;
	if (bytes / size != nmemb)
		return (NULL);
	ptr = malloc(bytes);
	if (!ptr)
		return (ptr);
	memset(ptr, 0, bytes);
	return (ptr);
}
