/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thugueno <thugueno@student.42angoulem      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/10 21:00:31 by thugueno          #+#    #+#             */
/*   Updated: 2023/07/01 17:29:19 by thugueno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

void	print_full(t_param *param)
{
	long	time;	

	time = get_timestamp_ms();
	ft_putnbr_fd(1, time - param->start);
	ft_putendl_fd(1, " Philosophers are full, simulation is over");
}

void	print_death(t_param *param, int philo)
{
	long	time;

	time = get_timestamp_ms();
	ft_putnbr_fd(1, time - param->start);
	ft_putchar_fd(1, ' ');
	ft_putnbr_fd(1, philo);
	ft_putendl_fd(1, " died");
}

long	get_timestamp_ms(void)
{
	struct timeval	tv;

	if (gettimeofday(&tv, NULL) == -1)
		return (-1);
	return ((tv.tv_sec * 1000) + (tv.tv_usec / 1000));
}

int	main(int argc, char **argv)
{
	t_observer	*observer;

	if (argc != 5 && argc != 6)
	{
		print_usage(argv[0]);
		return (1);
	}
	observer = init(argc, argv);
	if (!observer)
		return (1);
	observer->param->start = get_timestamp_ms();
	if (create_threads(observer->philos, observer->param->nb_philo))
	{
		ft_putendl_fd(2, "Error while initialize threads");
		free_observer(observer);
		return (1);
	}
	observer_routine(observer);
	join_threads(observer->philos, observer->param->nb_philo);
	free_observer(observer);
	return (0);
}
