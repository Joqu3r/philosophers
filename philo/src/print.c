/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thugueno <thugueno@student.42angouleme.fr  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/27 12:53:14 by thugueno          #+#    #+#             */
/*   Updated: 2023/06/27 07:25:47 by thugueno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

void	ft_putchar_fd(int fd, char c)
{
	write(fd, &c, 1);
	return ;
}

void	ft_putnbr_fd(int fd, long n)
{
	unsigned long	nb;
	char			c;

	if (n < 0)
	{
		ft_putchar_fd(fd, '-');
		n *= -1;
	}
	nb = n;
	if (nb > 9)
		ft_putnbr_fd(fd, nb / 10);
	c = nb % 10 + '0';
	ft_putchar_fd(fd, c);
}

void	ft_putstr_fd(int fd, char *str)
{
	int	i;

	if (!str)
		return ;
	i = 0;
	while (str[i])
	{
		ft_putchar_fd(fd, str[i]);
		i++;
	}
	return ;
}

void	ft_putendl_fd(int fd, char *str)
{
	if (!str)
		return ;
	ft_putstr_fd(fd, str);
	ft_putchar_fd(fd, '\n');
}
