/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error_handler.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thugueno <thugueno@student.42angouleme.fr  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/11 01:04:46 by thugueno          #+#    #+#             */
/*   Updated: 2023/06/30 13:01:50 by thugueno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

void	print_error(t_param *param, int argc)
{
	ft_putstr_fd(2, ERRCOLOR);
	if (param->nb_philo > MAX_PHILO || param->nb_philo == 0)
	{
		ft_putstr_fd(2, "Number of philosophers must be in range [1-");
		ft_putnbr_fd(2, MAX_PHILO);
		ft_putstr_fd(2, "]\n");
	}
	if ((param->t_todie > MAX_TIME || param->t_todie == 0)
		|| (param->t_toeat > MAX_TIME || param->t_toeat == 0)
		|| (param->t_tosleep > MAX_TIME || param->t_tosleep == 0))
	{
		ft_putstr_fd(2, "All time related argument must be in range [1-");
		ft_putnbr_fd(2, MAX_TIME);
		ft_putstr_fd(2, "]\n");
	}
	if (argc == 6 && param->musteat == 0)
	{	
		ft_putstr_fd(2, "Last argument must be in range [1-");
		ft_putnbr_fd(2, MAX_INT);
		ft_putstr_fd(2, "]\n");
	}
	ft_putstr_fd(2, NOCOLOR);
}

void	print_usage(char *name)
{
	ft_putstr_fd(2, "Usage: ");
	ft_putstr_fd(2, name);
	ft_putstr_fd(2, " <number_of_philosophers> from 1 to ");
	ft_putnbr_fd(2, MAX_PHILO);
	ft_putstr_fd(2, "\n\t\t<time_to_die> ");
	ft_putstr_fd(2, "\n\t\t<time_to_eat> ");
	ft_putstr_fd(2, "\n\t\t<time_to_sleep> ");
	ft_putstr_fd(2, "\n\t\t[number_of_times_each_philosopher_must_eat]");
	ft_putstr_fd(2, "\nTime is in millisecondes, from 1 to ");
	ft_putnbr_fd(2, MAX_TIME);
	ft_putstr_fd(2, "\nLast argument is optional, from 1 to ");
	ft_putnbr_fd(2, MAX_INT);
	ft_putchar_fd(2, '\n');
}
