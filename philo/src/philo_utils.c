/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo_utils.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thugueno <thugueno@student.42angouleme.fr  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/30 17:37:24 by thugueno          #+#    #+#             */
/*   Updated: 2023/06/30 23:01:26 by thugueno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

int	isdead(t_philo *philo)
{
	int	ret;

	pthread_mutex_lock(&philo->isdead.mutex);
	ret = philo->isdead.val;
	pthread_mutex_unlock(&philo->isdead.mutex);
	return (ret);
}

void	print_state(char *str, t_philo *philo)
{
	long	time;

	time = get_timestamp_ms() - philo->param->start;
	if (isdead(philo))
		return ;
	pthread_mutex_lock(&philo->param->voice);
	if (isdead(philo))
	{
		pthread_mutex_unlock(&philo->param->voice);
		return ;
	}
	ft_putnbr_fd(1, time);
	ft_putchar_fd(1, ' ');
	ft_putnbr_fd(1, philo->id);
	ft_putchar_fd(1, ' ');
	ft_putendl_fd(1, str);
	pthread_mutex_unlock(&philo->param->voice);
}
