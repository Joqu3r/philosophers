/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exit.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thugueno <thugueno@student.42angouleme.fr  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/06 12:59:15 by thugueno          #+#    #+#             */
/*   Updated: 2023/06/30 13:05:32 by thugueno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

void	free_philos(t_philo *philos, int limit)
{
	int	i;

	i = 0;
	while (i < limit)
	{
		pthread_mutex_destroy(&(philos[i].rfork));
		pthread_mutex_destroy(&(philos[i].isdead.mutex));
		pthread_mutex_destroy(&(philos[i].lasteat.mutex));
		i++;
	}
	free(philos);
}

void	free_observer(t_observer *observer)
{
	if (!observer)
		return ;
	if (observer->philos)
		free_philos(observer->philos, observer->param->nb_philo);
	pthread_mutex_destroy(&(observer->param->voice));
	pthread_mutex_destroy(&(observer->param->philof.mutex));
	free(observer->param);
	free(observer);
}
