/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thugueno <thugueno@student.42angoulem      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/29 08:03:31 by thugueno          #+#    #+#             */
/*   Updated: 2023/07/01 19:04:29 by thugueno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

static void	thinking(t_philo *philo)
{
	long	sleepms;

	print_state("is thinking", philo);
	if (philo->param->nb_philo % 2)
		sleepms = philo->param->t_toeat * 2;
	else
		sleepms = philo->param->t_toeat;
	sleepms -= philo->param->t_tosleep;
	usleep(sleepms * 1000);
}

static void	unlock_fork(t_philo *philo)
{
	pthread_mutex_unlock(philo->lfork);
	pthread_mutex_unlock(&philo->rfork);
}

static void	end_eat(t_philo *philo)
{
	pthread_mutex_lock(philo->lfork);
	if (isdead(philo))
		return (unlock_fork(philo));
	pthread_mutex_lock(&philo->lasteat.mutex);
	philo->lasteat.val = get_timestamp_ms() - philo->param->start;
	pthread_mutex_unlock(&philo->lasteat.mutex);
	print_state("has taken a fork", philo);
	print_state("is eating", philo);
	usleep(philo->param->t_toeat * 1000);
	unlock_fork(philo);
	if (isdead(philo))
		return ;
	philo->t_eaten += 1;
	if (philo->t_eaten == philo->param->musteat)
	{
		pthread_mutex_lock(&philo->param->philof.mutex);
		philo->param->philof.val += 1;
		pthread_mutex_unlock(&philo->param->philof.mutex);
	}
}

static int	start_eat(t_philo *philo)
{
	pthread_mutex_lock(&philo->rfork);
	print_state("has taken a fork", philo);
	if (isdead(philo))
	{
		pthread_mutex_unlock(&philo->rfork);
		return (0);
	}
	if (!philo->lfork)
	{
		pthread_mutex_unlock(&philo->rfork);
		usleep(philo->param->t_todie * 1000);
		if (isdead(philo))
			return (0);
	}
	else
	{
		end_eat(philo);
		if (isdead(philo))
			return (0);
	}
	return (1);
}

void	*philo_routine(void *arg)
{
	t_philo	*philo;

	philo = arg;
	while (isdead(philo) == 0)
	{
		if (!start_eat(philo))
			break ;
		print_state("is sleeping", philo);
		if (isdead(philo))
			break ;
		usleep(philo->param->t_tosleep * 1000);
		if (isdead(philo))
			break ;
		thinking(philo);
	}
	return (philo);
}
