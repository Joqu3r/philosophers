/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   thread.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thugueno <thugueno@student.42angoulem      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/29 08:03:59 by thugueno          #+#    #+#             */
/*   Updated: 2023/07/01 17:17:15 by thugueno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

void	join_threads(t_philo *philos, int nb_philo)
{
	int	i;

	i = 0;
	while (i < nb_philo)
	{
		pthread_join(philos[i].thread, NULL);
		i++;
	}
}

int	create_threads(t_philo *philos, int nb_philo)
{
	int	i;
	int	y;

	i = 0;
	while (i < nb_philo)
	{
		pthread_create(&philos[i].thread, NULL, &philo_routine, &philos[i]);
		usleep(1000);
		i += 2;
	}
	y = 1;
	while (y < nb_philo)
	{
		pthread_create(&philos[y].thread, NULL, &philo_routine, &philos[y]);
		if (y != (nb_philo - 1))
			usleep(1000);
		y += 2;
	}
	return (0);
}
